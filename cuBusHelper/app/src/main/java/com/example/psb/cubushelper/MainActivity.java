// 300CEM Android Applications Development
// Creator: Lim junhyeok(G1557834W)

package com.example.psb.cubushelper;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

// Set up to work with Google Map, add callback function connection
// Location information is registered and configured, and the location information is linked in real time
public class MainActivity extends AppCompatActivity
        implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    // Variable: Google map management
    private GoogleApiClient mGoogleApiClient = null;
    private GoogleMap mGoogleMap = null;
    private Marker mCurrentMarker = null;

    // Variable: tag to attach to log message
    private static final String TAG = "Limjunhyeok";

    // Variables: Settings for allowing access to Android location information and updating location information
    private static final int GPS_ENABLE_REQUEST_CODE = 2001;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 2002;
    private static final int UPDATE_INTERVAL_MS = 1000;         // Update location information once per second
    private static final int FASTEST_UPDATE_INTERVAL_MS = 500;  // Update location information once every 0.5 seconds if a fast update is needed

    // Variable: Value for accessing Android location information (permission) setting and status management
    private AppCompatActivity mActivity;
    private boolean mAskPermissionOnceAgain = false;

    // Variable: Value for managing real-time location update
    private static final double DISTANCE_CHECK_VALUE = 30.0;  // Recognize as arrival if approach within 30 meters (considering movement speed, map accumulation)
    boolean mRequestingLocationUpdates = false;
    //Location mCurrentLocatiion;   // If you need an old location, use it.
    boolean mMoveMapByUser = true;
    boolean mMoveMapByAPI = true;
    LatLng mCurrentPosition;
    private static Toast mToast = null;

    LocationRequest mLocationRequest = new LocationRequest()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setInterval(UPDATE_INTERVAL_MS)
            .setFastestInterval(FASTEST_UPDATE_INTERVAL_MS);

    // Variable: Value for setting and managing pre-registered position information
    private List<MarkerOptions> mPositionMarkers = null;
    private int mUserPostion = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // When running the app, make sure the screen is always on
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(com.example.psb.cubushelper.R.layout.activity_main);

        Log.d(TAG, "onCreate");
        mActivity = this;

        // Connect Google API screen configuration to Activity and configure it to work together
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(com.example.psb.cubushelper.R.id.map);
        mapFragment.getMapAsync(this);
    }

    // Add menu to ActionBar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    // It is executed when menu is clicked.
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        // Show the About window as Pop.
        if (id == R.id.action_menu_about) {
            showAbout();
            return true;
        }
        else // Exit the program
        if (id == R.id.action_menu_exit) {
            confirmAndExit();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // Function: Pop up the About window.
    void showAbout() {
        // Call popup (activity) with data
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    // Function: Contact the user and close the program (app) when agreed
    void confirmAndExit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.app_name);
        builder.setMessage(R.string.msg_exit_confirm);
        builder.setPositiveButton(R.string.no,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Do nothing.
                    }
                });
        builder.setNegativeButton(R.string.yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finishAffinity();
                        System.runFinalization();
                        System.exit(0);
                    }
                });
        builder.show();
    }

    @Override
    public void onResume() {
        super.onResume();

        // Activity is Resume and starts to update location information when Google API is connected.
        if (mGoogleApiClient.isConnected()) {
            Log.d(TAG, "onResume : call startLocationUpdates");
            if (!mRequestingLocationUpdates) startLocationUpdates();
        }

        // Check again to see if you have Permission for app operation.
        if (mAskPermissionOnceAgain) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mAskPermissionOnceAgain = false;
                checkPermissions();
            }
        }
    }

    // Function: Update the information on the MAP using the current location information.
    private void startLocationUpdates() {

        // If the location information is unavailable, proceed with setting up the relevant information.
        if (!checkLocationServicesStatus()) {
            Log.d(TAG, "startLocationUpdates : call showDialogForLocationServiceSetting");
            showDialogForLocationServiceSetting();
        }else {
            // If the necessary permissions are not secured, output the relevant information and terminate the function. (To check again)
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "startLocationUpdates : Has no permissions");
                return;
            }

            // The location information update is started.
            Log.d(TAG, "startLocationUpdates : call requestLocationUpdates");
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            mRequestingLocationUpdates = true;
            mGoogleMap.setMyLocationEnabled(true);
        }

    }

    // Function: Finish updating the location information.
    private void stopLocationUpdates() {
        Log.d(TAG,"stopLocationUpdates : LocationServices.removeLocationUpdates");
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        mRequestingLocationUpdates = false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "onMapReady :");

        mGoogleMap = googleMap;

        // The position information is normally retrieved and the initial position value is determined and set before setting the current position.
        setDefaultLocation();

        // Setting the behavior properties of Google Maps.
        //mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(15));

        // Specifying the Callback function for Google Map interaction
        mGoogleMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener(){
            @Override
            public boolean onMyLocationButtonClick() {
                Log.d( TAG, "onMyLocationButtonClick");
                mMoveMapByAPI = true;
                return true;
            }
        });
        mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                Log.d( TAG, "onMapClick: " + latLng.latitude + ", " + latLng.longitude);
            }
        });
        mGoogleMap.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
            @Override
            public void onCameraMoveStarted(int i) {
                if (mMoveMapByUser == true && mRequestingLocationUpdates){
                    Log.d(TAG, "onCameraMoveStarted : Disable");
                    mMoveMapByAPI = false;
                }
                mMoveMapByUser = true;
            }
        });
        mGoogleMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
            }
        });

        // Displays position information configured from Position Data.
        // Inside the function, Postion information is constructed from the position information file using PosInfo Class.
        setUserPositionMarker();
    }


    @Override
    public void onLocationChanged(Location location) {

        mCurrentPosition
                = new LatLng( location.getLatitude(), location.getLongitude());

        // Organize information for present location information
        String markerTitle = getCurrentAddress(mCurrentPosition);
        String markerSnippet = "lat:" + String.valueOf(location.getLatitude())
                + " long:" + String.valueOf(location.getLongitude());
        Log.d(TAG, "onLocationChanged : " + markerTitle + " " + markerSnippet);

        // Creates a marker at the current position. (I.e., move the current position)
        setCurrentLocation(location, markerTitle, markerSnippet);

        // If you need an old location, use it.
        //mCurrentLocatiion = location;

        // And displays the 'entry and movement' information in comparison with the user-defined location information.
        CheckLocation(location);
    }

    @Override
    protected void onStart() {
        if(mGoogleApiClient != null && mGoogleApiClient.isConnected() == false){

            Log.d(TAG, "onStart: call mGoogleApiClient.connect");
            mGoogleApiClient.connect();
        }
        super.onStart();
    }

    @Override
    protected void onStop() {
        if (mRequestingLocationUpdates) {
            Log.d(TAG, "onStop : call stopLocationUpdates");
            stopLocationUpdates();
        }
        if (mGoogleApiClient.isConnected()) {

            Log.d(TAG, "onStop : mGoogleApiClient.disconnect");
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        // Check (check and request) Android location information permission and link with Google map.
        if ( mRequestingLocationUpdates == false ) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                int hasFineLocationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
                if (hasFineLocationPermission == PackageManager.PERMISSION_DENIED) {
                    ActivityCompat.requestPermissions(mActivity,
                            new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                            PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
                } else {
                    Log.d(TAG, "onConnected : already has permissions");
                    Log.d(TAG, "onConnected : call startLocationUpdates");
                    startLocationUpdates();
                    mGoogleMap.setMyLocationEnabled(true);
                }
            }else{
                Log.d(TAG, "onConnected : call startLocationUpdates");
                startLocationUpdates();
                mGoogleMap.setMyLocationEnabled(true);
            }
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed");
        // When the location information is normally retrieved and the current location is not available, the location information corresponding to the initial location value is used.
        setDefaultLocation();
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.d(TAG, "onConnectionSuspended");
        if (cause == CAUSE_NETWORK_LOST)
            Log.e(TAG, "onConnectionSuspended(): Google Play services " +
                    "connection lost.  Cause: network lost.");
        else if (cause == CAUSE_SERVICE_DISCONNECTED)
            Log.e(TAG, "onConnectionSuspended():  Google Play services " +
                    "connection lost.  Cause: service disconnected");
    }

    // Function: Obtains address information using location information.
    public String getCurrentAddress(LatLng latlng) {

        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses;

        try {
            addresses = geocoder.getFromLocation(latlng.latitude, latlng.longitude, 1);
        } catch (IOException ioException) {
            // Network problems
            Toast.makeText(this, "Geocoder service is not available", Toast.LENGTH_LONG).show();
            return "Geocoder service is not available";
        } catch (IllegalArgumentException illegalArgumentException) {
            Toast.makeText(this, "Invalid location", Toast.LENGTH_LONG).show();
            return "Invalid location";
        }

        if (addresses == null || addresses.size() == 0) {
            Toast.makeText(this, "Address is not found", Toast.LENGTH_LONG).show();
            return "Address is not found";

        } else {
            Address address = addresses.get(0);
            return address.getAddressLine(0).toString();
        }
    }

    // Function: Return location info service status
    public boolean checkLocationServicesStatus() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
                || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    // Function: Sets the current location information on the Google map. (marker setting)
    public void setCurrentLocation(Location location, String markerTitle, String markerSnippet) {

        mMoveMapByUser = false;

        if (mCurrentMarker != null) mCurrentMarker.remove();

        LatLng currentLatLng = new LatLng(location.getLatitude(), location.getLongitude());

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(currentLatLng);
        markerOptions.title(markerTitle);
        markerOptions.snippet(markerSnippet);
        markerOptions.draggable(true);
        markerOptions.icon(BitmapDescriptorFactory.fromResource(com.example.psb.cubushelper.R.drawable.bus_48));

        mCurrentMarker = mGoogleMap.addMarker(markerOptions);

        if (mMoveMapByAPI) {
            Log.d( TAG, "setCurrentLocation : mGoogleMap moveMapByAPI "
                    + location.getLatitude() + " " + location.getLongitude() ) ;
            // Note: If necessary, you can set Google Maps zoom and pan
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLng(currentLatLng);
            mGoogleMap.animateCamera(cameraUpdate);

        }
    }

    // Function: If there is a problem with location information or Google map linkage, configure basic location information. The required attributes use default values.
    public void setDefaultLocation() {

        mMoveMapByUser = false;

        // ToDo: The basic position information is determined and applied. Currently, Singapore's EST Solutions Asia Pte Ltd is the primary location.
        LatLng DEFAULT_LOCATION = new LatLng(1.332662,103.902292);
        String markerTitle = "EST Solutions Asia Pte Ltd";
        String markerSnippet = "Inital position - Please, check GPS status";

        if (mCurrentMarker != null) mCurrentMarker.remove();

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(DEFAULT_LOCATION);
        markerOptions.title(markerTitle);
        markerOptions.snippet(markerSnippet);
        markerOptions.draggable(true);
        markerOptions.icon(BitmapDescriptorFactory.fromResource(com.example.psb.cubushelper.R.drawable.bus_48));
        mCurrentMarker = mGoogleMap.addMarker(markerOptions);

        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(DEFAULT_LOCATION, 15);
        mGoogleMap.moveCamera(cameraUpdate);

    }

    // Function: Configures user-defined position information in conjunction with Google Map.
    public void setUserPositionMarker() {

        PosInfo posInfo = new PosInfo();

        if (false == posInfo.processFile(getBaseContext())) {
            Log.d( TAG, "setUserPositionMarker: posInfo.processFile() failed");
            return;
        }

        mPositionMarkers = new ArrayList<MarkerOptions>();

        for (int pos = 0; pos < posInfo.postionCount(); pos++) {
            Position position = posInfo.getPosition(pos);

            MarkerOptions marker = new MarkerOptions();
            LatLng latlng = new LatLng(position.mLatitude, position.mLongitude);
            marker.position(latlng);
            marker.title(position.mTitle);
            marker.snippet(position.mSnippet);
            mPositionMarkers.add(marker);

            mGoogleMap.addMarker(marker);

            Log.d( TAG, "a UserPosition added: "
                    + marker.getTitle() + ", " + marker.getSnippet() + ", "
                    + marker.getPosition().latitude + ", " + marker.getPosition().longitude);
        }

        mUserPostion = -1;
    }

    // Function: To reuse and display Toast Message.
    private void makeToast(final String text, final int duration) {
        if (null == mToast) {
            mToast = Toast.makeText(this, "", Toast.LENGTH_LONG);
        }
        mToast.setText(text);
        mToast.setDuration(duration);
        mToast.show();
    }

    // Function: Compares current position information with user registration position and displays 'entry direction, direction'.
    // Assumption: The bus shall be moved in a single direction from the starting point to the end point (end point).
    public void CheckLocation(Location location) {

        double distanceInMeters = 0;
        LatLng tmpPoint;
        Location locationMarker = new Location("");
        MarkerOptions tmpMarker;

        boolean bArrived = false;

        int pos = 0;
        for (pos = (-1 == mUserPostion) ? 0 : mUserPostion; pos < mPositionMarkers.size() ; pos++)
        {
            tmpMarker = mPositionMarkers.get(pos);
            tmpPoint = tmpMarker.getPosition();
            locationMarker.setLatitude(tmpPoint.latitude);
            locationMarker.setLongitude(tmpPoint.longitude);
            distanceInMeters = locationMarker.distanceTo(location);

            Log.d( TAG, "CheckLocation " + pos + " : " + tmpMarker.getTitle() + " => " + distanceInMeters);

            if(distanceInMeters < DISTANCE_CHECK_VALUE)
            {
                bArrived = true;
                mUserPostion = pos;
                makeToast("You are arrived in " + tmpMarker.getTitle(), Toast.LENGTH_LONG);
                break;
            }
        }

        // If it is not in a specific position, the direction is indicated.
        if (false == bArrived) {
            if (mPositionMarkers.size() -1 <= mUserPostion) {
                makeToast("You have arrived at the terminal.", Toast.LENGTH_LONG);
            }
            else {
                makeToast("You are going to the " + mPositionMarkers.get(mUserPostion+1).getTitle() + ".", Toast.LENGTH_LONG);
            }
        }
    }


    // Functions: functions for managing Android permissions (permissions), since
    @TargetApi(Build.VERSION_CODES.M)
    private void checkPermissions() {
        boolean fineLocationRationale = ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int hasFineLocationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

        if (hasFineLocationPermission == PackageManager.PERMISSION_DENIED && fineLocationRationale)
            showDialogForPermission("Please grant Android permissions to run services.");
        else if (hasFineLocationPermission == PackageManager.PERMISSION_DENIED && !fineLocationRationale) {
            showDialogForPermissionSetting("Deny permissions and don't ask again. " + "Please grant permissions with checkbox setting.");
        } else if (hasFineLocationPermission == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "checkPermissions : already granted");
            if ( mGoogleApiClient.isConnected() == false) {
                Log.d(TAG, "checkPermissions : call mGoogleApiClient.connect");
                mGoogleApiClient.connect();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (permsRequestCode == PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION && grantResults.length > 0) {
            boolean permissionAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

            if (permissionAccepted) {
                if ( mGoogleApiClient.isConnected() == false) {
                    Log.d(TAG, "onRequestPermissionsResult : mGoogleApiClient connect");
                    mGoogleApiClient.connect();
                }
            } else {
                checkPermissions();
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void showDialogForPermission(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Notification");
        builder.setMessage(msg);
        builder.setCancelable(false);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ActivityCompat.requestPermissions(mActivity,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        });
        builder.create().show();
    }

    private void showDialogForPermissionSetting(String msg) {

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Notification");
        builder.setMessage(msg);
        builder.setCancelable(true);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                mAskPermissionOnceAgain = true;

                Intent myAppSettings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.parse("package:" + mActivity.getPackageName()));
                myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
                myAppSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mActivity.startActivity(myAppSettings);
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        });
        builder.create().show();
    }

    // Function: Since, the function for GPS activation
    private void showDialogForLocationServiceSetting() {

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Position service disabled");
        builder.setMessage("Postion service is required to use the SingBus.\n"
                + "Do you want to enable settings?");
        builder.setCancelable(true);
        builder.setPositiveButton("Setting", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                Intent callGPSSettingIntent
                        = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(callGPSSettingIntent, GPS_ENABLE_REQUEST_CODE);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        builder.create().show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case GPS_ENABLE_REQUEST_CODE:
                // Make sure the user has enabled GPS
                if (checkLocationServicesStatus()) {
                    if (checkLocationServicesStatus()) {
                        Log.d(TAG, "onActivityResult : Permission granted");
                        if ( mGoogleApiClient.isConnected() == false ) {
                            Log.d( TAG, "onActivityResult : call mGoogleApiClient.connect ");
                            mGoogleApiClient.connect();
                        }
                        return;
                    }
                }
                break;
            default:
                break;
        }
    }
}