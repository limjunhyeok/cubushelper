// 300CEM Android Applications Development
// Creator: Lim junhyeok(G1557834W)

package com.example.psb.cubushelper;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;

public class AboutActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // To show as a pop-up, remove the title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_about);
    }

    // Do not close it if you click outside the pop-up.
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(event.getAction()==MotionEvent.ACTION_OUTSIDE){
            return false;
        }
        return true;
    }

    // Prevent Android back button. That is, it is configured to close only with the close button.
    @Override
    public void onBackPressed() {
        return;
    }

    // Function: Click Close button
    public void onClose(View v){
        finish();
    }
}
