// 300CEM Android Applications Development
// Creator: Lim junhyeok(G1557834W)

package com.example.psb.cubushelper;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

// One location information set in the JSON file
class Position {
    public String mTitle = null;
    public String mSnippet = null;
    public double mLatitude = 0.0;
    public double mLongitude = 0.0;
}

// A class that creates and provides location information by reading data from a JSON file
// (1) Read data from predefined external files
// (2) Reading data from an internal resource file when a diary fails in an external file
class PosInfo {

    // Constants: external file name, internal resource name
    private final String POSITION_DATA_FILENAME = "position.dat";
    private final int POSITION_DATA_ID = com.example.psb.cubushelper.R.raw.position;

    // Variable: Position information is managed by Array (List)
    private ArrayList mPositionArray = null;

    // Function: Returns the number of position information
    public int postionCount() {
        return (null != mPositionArray) ? mPositionArray.size() : 0;
    }

    // Function: Returns position information corresponding to Index (order)
    public Position getPosition(int pos) {
        return (null != mPositionArray && pos < mPositionArray.size()) ? (Position)mPositionArray.get(pos) : null;
    }

    // Function: Read location of pre-defined JOSN file. Returns TRUE if more than one location is configured
    public boolean processFile(Context context)
    {
        String jsonStr = readRawTextFile(context, this.POSITION_DATA_FILENAME);
        if (null == jsonStr)
            jsonStr = readRawTextFile(context, this.POSITION_DATA_ID);

        if (null == jsonStr)
            return false;

        try {
            mPositionArray = parseJsonFromString(jsonStr);
        } catch(Exception e){
            mPositionArray = null;
            e.printStackTrace();
        }

        return (boolean)(null != mPositionArray && false == mPositionArray.isEmpty());
    }

    // Function: Check if external storage is readable or mounted
    private boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    // Function: Read JSON file and return it as string
    private String readRawTextFile(Context context, String filename)
    {
        if (false == isExternalStorageReadable()) {
            Toast.makeText(context, "External Storage is not readable.", Toast.LENGTH_LONG).show();
            return null;
        }

        // Check the Write Permission. If necessary, set it manually.
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(context, "READ_EXTERNAL_STORAGE permission not granted.\nPlease, grant it manually", Toast.LENGTH_LONG).show();
            return null;
        }

        String dataFile = Environment.getExternalStorageDirectory() + "/" + filename;

        File file = new File(dataFile);
        if (null == file || false == file.exists() || false == file.canRead()) {
            Toast.makeText(context, "Data file is not readable.\nPlease, check position file: " + dataFile, Toast.LENGTH_LONG).show();
            return null;
        }

        try {
            InputStream inputStream = new FileInputStream(file);
            if (null == inputStream)
                return null;

            InputStreamReader inputreader = new InputStreamReader(inputStream);
            BufferedReader buffreader = new BufferedReader(inputreader);
            String line;
            StringBuilder text = new StringBuilder();

            try {
                while ((line = buffreader.readLine()) != null) {
                    text.append(line);
                }
            } catch (IOException e) {
                return null;
            }
            return text.toString();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    private String readRawTextFile(Context context, int resID)
    {
        InputStream inputStream = context.getResources().openRawResource(resID);
        if (null == inputStream)
            return null;

        Toast.makeText(context, "Internal data file used", Toast.LENGTH_LONG).show();

        InputStreamReader inputreader = new InputStreamReader(inputStream);
        BufferedReader buffreader = new BufferedReader(inputreader);
        String line;
        StringBuilder text = new StringBuilder();

        try {
            while (( line = buffreader.readLine()) != null) {
                text.append(line);
            }
        } catch (IOException e) {
            return null;
        }

        return text.toString();
    }

    // Function: Construct and return Position information in JSON string
    private ArrayList parseJsonFromString(String jsonString)
            throws JSONException {

        ArrayList contentList = new ArrayList();

        try {
            JSONObject contentJson = new JSONObject(jsonString);


            JSONArray itemArray = contentJson.getJSONArray("positions");

            for(int i = 0; i < itemArray.length(); i++) {

                Position position = new Position();
                JSONObject itemDetails = itemArray.getJSONObject(i);
                try {
                    if (!itemDetails.isNull("title")) {
                        position.mTitle = itemDetails.getString("title");
                    }
                    if (!itemDetails.isNull("snippet")) {
                        position.mSnippet = itemDetails.getString("snippet");
                    }
                    if (!itemDetails.isNull("latitude")) {
                        position.mLatitude = Double.parseDouble(itemDetails.getString("latitude"));
                    }
                    if (!itemDetails.isNull("longitude")) {
                        position.mLongitude = Double.parseDouble(itemDetails.getString("longitude"));
                    }
                } catch (NumberFormatException nfe) {
                    System.err.println(nfe);
                }

                contentList.add(position);
            }

        } catch (JSONException e) {
            contentList = null;
            e.printStackTrace();
        }

        return contentList;
    }
}